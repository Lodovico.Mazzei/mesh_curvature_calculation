#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 30 13:16:15 2024

@author: mazzei
"""

# Import necessary libraries
import pyvista as pv
import numpy as np
import os


def getPointDistances(points):
    """
    Computes the sum of vector differences between each point and every other point.
    
    Parameters:
    points (ndarray): An array of points (Nx3), where N is the number of points.

    Returns:
    ndarray: An array of summed vector differences (Nx3).
    """
    diff_vectors = points[:, np.newaxis, :] - points[np.newaxis, :, :]
    sum_diff_vectors = np.sum(diff_vectors, axis=1)
    return sum_diff_vectors


def get_edges_ids(first, second, third):
    """
    Computes unique edge IDs for the edges of a triangle using Cantor pairing.
    
    Parameters:
    first, second, third (int): Vertex IDs of the triangle.

    Returns:
    ndarray: Array of edge IDs.
    """
    edge1 = tuple(np.sort([first, second]))
    edge2 = tuple(np.sort([second, third]))
    edge3 = tuple(np.sort([third, first]))
    
    return [edge1, edge2, edge3]


def add_edges_ids(edges_to_triangles, edges_ids, tr_id): 
    """
    Adds triangle ID to a dictionary where edges are keys and associated triangles are values.
    
    Parameters:
    edges_to_triangles (dict): Dictionary mapping edges to triangle IDs.
    edges_ids (ndarray): Array of edge IDs.
    tr_id (int): Triangle ID.
    """
    for edge_id in edges_ids:
        if edge_id not in edges_to_triangles.keys():
            edges_to_triangles[edge_id] = []
        edges_to_triangles[edge_id].append(tr_id)


def cantorPairing(vec, two_D=False):
    """
    Generates a unique ID for a vector in 2D or 3D space using Cantor pairing.

    Parameters:
    vec (ndarray): Array of vertex IDs.
    two_D (bool): Whether to generate a 2D ID or 3D ID.

    Returns:
    int: Unique ID for the vector.
    """
    ab = (vec[0]+vec[1])*(vec[0]+vec[1]+1)/2 + vec[1]
    if two_D:
        abc = ab
    else:
        abc = (ab+vec[2])*(ab+vec[2]+1)/2 + vec[2]
    return int(abc)


def reorder_triangle_vertices(triangles):
    """
    Ensures that all triangles have their vertices ordered consistently to maintain 
    outward-facing normals.
    
    Parameters:
    triangles (list): List of triangles (each a list of three vertex indices).

    Returns:
    ndarray: Array of triangles with consistent vertex ordering.
    """
    from collections import defaultdict, deque

    # Build adjacency list for edges
    edge_to_triangles = defaultdict(list)
    for idx, t in enumerate(triangles):
        edges = [(t[0], t[1]), (t[1], t[2]), (t[2], t[0])]
        for edge in edges:
            edge_to_triangles[tuple(sorted(edge))].append(idx)

    # Set to keep track of processed triangles
    processed = set()
    queue = deque([0])
    processed.add(0)

    while queue:
        j = queue.popleft()
        t = triangles[j]
        couples = [[t[0], t[1]], [t[1], t[2]], [t[2], t[0]]]

        for c in couples:
            sorted_c = tuple(sorted(c))
            for oth_id in edge_to_triangles[sorted_c]:
                if oth_id == j or oth_id in processed:
                    continue

                oth_t = triangles[oth_id]

                # Reverse vertices in the other triangle if necessary
                if c == [oth_t[0], oth_t[1]]:
                    triangles[oth_id] = np.array([oth_t[1], oth_t[0], oth_t[2]])
                elif c == [oth_t[1], oth_t[2]]:
                    triangles[oth_id] = np.array([oth_t[0], oth_t[2], oth_t[1]])
                elif c == [oth_t[2], oth_t[0]]:
                    triangles[oth_id] = np.array([oth_t[2], oth_t[1], oth_t[0]])

                queue.append(oth_id)
                processed.add(oth_id)

    return triangles


def triangleInfo(A, B, C):
    """
    Calculates the covariant and contravariant edge vectors, metric, and inverse metric
    for a given triangle.

    Parameters:
    A, B, C (ndarray): 3D coordinates of the triangle's vertices.

    Returns:
    list: Contains edge vectors, normal vector, contravariant vectors, 
          area, metric tensor, and inverse metric tensor.
    """
    v1 = np.subtract(B, A)
    v2 = np.subtract(C, A)
    G = np.array([[np.dot(v1, v1), np.dot(v1, v2)], [np.dot(v2, v1), np.dot(v2, v2)]])
    try:
        Ginv = np.linalg.inv(G)
    except np.linalg.LinAlgError as e:
        Ginv = np.full((2,2), np.nan)
        #print(f"Error: {G}")
    cv1 = Ginv[0, 0] * v1 + Ginv[0, 1] * v2
    cv2 = Ginv[1, 0] * v1 + Ginv[1, 1] * v2
    cross = np.cross(v1, v2)
    n = cross / np.linalg.norm(cross)
    area = np.linalg.norm(cross) / 2
    return [v1, v2, n, cv1, cv2, area, G, Ginv]


def get_curvature(tr, tr_dict, neighs_order):
    
    """
    Calculate the curvature at the barycenter of a triangle.
    
    Parameters:
    tr (int): Triangle ID.
    tr_dict (dict): Dictionary containing triangle information.
    neighs_order: will use 1st or 2nd order neighbours of triangle to compute curvature (default 2nd order)
    
    Returns:
    list: Various computed metrics including metric tensor, curvature tensor,
          mean curvature, Gaussian curvature, eigenvalues, and eigenvectors.
    """
    # Initialize array for curvature tensor
    C = np.zeros((2,2))
   
    # extract bary, s, phi
    tr_bary = tr_dict[tr]["barycenter"]
    # extract e1, e2, ce1, ce2, n
    e1, e2 = tr_dict[tr]["e1"], tr_dict[tr]["e2"]
    ce1, ce2 = tr_dict[tr]["ce1"], tr_dict[tr]["ce2"]
    n = tr_dict[tr]["n"]
        
    # get metrics
    g = tr_dict[tr]["g"]
    g_inv = tr_dict[tr]["g_inv"]
    g_3D = g[0,0]*np.outer(ce1,ce1) + g[0,1]*np.outer(ce1,ce2) + g[1,0]*np.outer(ce2,ce1) + g[1,1]*np.outer(ce2,ce2)
 
    # array to store curvature
    C = np.zeros((2,2))
    # lists for distance components with neighbours
    z_nbs, xi_nbs, eta_nbs = [],[],[]
    
    # checking whether metric is not invertible or triangle has more/less than 3 neighbours
    if np.isnan(g_inv).any() or len(tr_dict[tr]["neighbors"]) != 3:
        C, C_mix, C_3D, C_mean, C_gauss, evals, evecs = np.full((2,2), np.nan), np.full((2,2), np.nan), np.full((3,3), np.nan), np.nan, np.nan, np.nan, np.zeros((2,3), np.nan)
    
    # solving the system
    else:
        # 1st ORDER NEIGHBORS
        if neighs_order == 1:
            for nb in tr_dict[tr]["neighbors"]:
                dr = tr_dict[nb]["barycenter"] - tr_bary
                # neighbours' bary normal component
                z_nb = np.dot(dr, n)
                z_nbs.append(z_nb)
                # neighbors bary xi and eta components
                xi_nb = np.dot(dr, ce1)
                xi_nbs.append(xi_nb)
                eta_nb = np.dot(dr, ce2)
                eta_nbs.append(eta_nb)
            
            # solve linear system of equations
            S = np.array([[xi_nbs[0]**2, xi_nbs[0]*eta_nbs[0], eta_nbs[0]**2],
                          [xi_nbs[1]**2,  xi_nbs[1]*eta_nbs[1], eta_nbs[1]**2],
                          [xi_nbs[2]**2, xi_nbs[2]*eta_nbs[2], eta_nbs[2]**2]])
            b = np.array([z_nbs[0], z_nbs[1], z_nbs[2]])
            v = np.linalg.solve(S, b) 
            
        
        # 2nd ORDER NEIGHBORS
        elif neighs_order == 2:
            
            ##### NEXT NEAREST NEIGHBOURS ########
            visited = set() 
            for nb in tr_dict[tr]["neighbors"]:
                if nb not in visited:
                    visited.add(nb)
                    dr = tr_dict[nb]["barycenter"] - tr_bary
                    # Neighbors' barycenter normal component
                    z_nb = np.dot(dr, n)
                    z_nbs.append(z_nb)
                    # Neighbors' barycenter xi and eta components
                    xi_nb = np.dot(dr, ce1)
                    xi_nbs.append(xi_nb)
                    eta_nb = np.dot(dr, ce2)
                    eta_nbs.append(eta_nb)
            
                # Loop through second-order neighbors
                for nb2 in tr_dict[nb]["neighbors"]:
                    if nb2 not in visited:
                        visited.add(nb2)
                        dr2 = tr_dict[nb2]["barycenter"] - tr_bary
                        # Second-order neighbors' barycenter normal component
                        z_nb2 = np.dot(dr2, n)
                        z_nbs.append(z_nb2)
                        # Second-order neighbors' barycenter xi and eta components
                        xi_nb2 = np.dot(dr2, ce1)
                        xi_nbs.append(xi_nb2)
                        eta_nb2 = np.dot(dr2, ce2)
                        eta_nbs.append(eta_nb2)
                    
            S = np.array([[xi**2, xi * eta, eta**2] for xi, eta in zip(xi_nbs, eta_nbs)])
            b = np.array(z_nbs)

            # Use np.linalg.lstsq to solve the overconstrained system
            v, residuals, rank, s_lstq = np.linalg.lstsq(S, b, rcond=None)
              
        # covariant components C_ij
        C[0,0] = -2*v[0]
        C[0,1] = -v[1]
        C[1,0] = -v[1]
        C[1,1] = -2*v[2]
        
        # mixed component curvature
        C_mix = np.dot(C, g_inv)
        
        # 3D curvature tensor
        C_3D = C[0,0]*np.outer(ce1,ce1) + C[0,1]*np.outer(ce1,ce2) + C[1,0]*np.outer(ce2,ce1) + C[1,1]*np.outer(ce2,ce2)
        
        # calculating eigenvalues and eigenvectors of \tilde{C}_ij 
        C_tilde = C_mix - np.trace(C_mix)/2
        eigen = np.linalg.eig(C_tilde)
        evals = np.array([eigen[0][np.argmin(eigen[0])], eigen[0][np.argmax(eigen[0])]])
        evecs_2d = np.array([eigen[1][:,np.argmin(eigen[0])], eigen[1][:,np.argmax(eigen[0])]])
        
        evecs = np.zeros(shape=(2,3))
        for i in range(2):
            evecs[i] = (evecs_2d[i][0]*ce1 + evecs_2d[i][1]*ce2)/np.linalg.norm(evecs_2d[i][0]*ce1 + evecs_2d[i][1]*ce2)
        
        # calculating mean and Gaussian curvatures
        C_mean = np.trace(C_mix)/2
        C_gauss = np.linalg.det(C)/np.linalg.det(g)
            
    
    return[g_3D, C, C_mix, C_3D, C_mean, C_gauss, evals, evecs]

# stores metric, curvature, eigenvals and eigenvecs in the triangle dictionary           
def set_curvatures(tr_dict, neighs_order):
    for tr in list(tr_dict.keys()):
        g_3D, C, C_mix, C_3D, C_mean, C_gauss, evals, evecs = get_curvature(tr, tr_dict, neighs_order)
        
        tr_dict[tr].update({
            "g_3D": g_3D,
            "C": C,
            "C_mix": C_mix,
            "C_3D": C_3D,
            "C_mean": C_mean,
            "C_gauss": C_gauss,
            "evals": evals,
            "evecs": evecs,
            })
        

# main calculation function
def get_mesh_info(vtk_mesh_path, neighs_order=2, save_vtk=False, vtk_name=False):
    """
    Reads a VTK mesh file, computes geometric and curvature information for each triangle,
    and optionally saves the updated mesh with additional information in a specified directory.

    Parameters:
    vtk_mesh_path (str): Path to the input VTK file.
    save_vtk (bool): Whether to save the updated mesh and curvature information to new VTK files.
    vtk_name (str or bool): Custom name for the output directory. If False, use default names.

    Returns:
    dict: Dictionary containing detailed information about each triangle in the mesh.
    """
    try:
        # Read the VTK mesh
        vtk_mesh = pv.read(vtk_mesh_path)
    except Exception as e:
        raise RuntimeError(f"Failed to read VTK file: {e}")
    
    # Extract points (vertices) and cells (triangles)
    points = vtk_mesh.points
    faces = vtk_mesh.faces
    
    # Reorder triangle vertices so that all normals point outwards
    triangles = []
    i = 0
    while i < len(faces):
        n = faces[i]  # Number of points in this face (should be 3 for triangles)
        triangles.append(faces[i+1:i+1+n])
        i += n + 1  # Move to the next face
    triangles = reorder_triangle_vertices(triangles)
    
    # Storage variables
    tr_dict, edges_to_triangles = {}, {}
    tr_bary_list, normals = [], []
    
    # Loop over triangles and calculate preliminary quantities
    for tr in triangles:
        # IDs of the triangle's vertices
        first, second, third = tr[0], tr[1], tr[2]
        
        # 3D-coordinates of the 3 vertices
        locationA, locationB, locationC = np.array(points[first]), np.array(points[second]), np.array(points[third])
            
        # Calculate triangle barycenter
        tr_bary = np.mean([locationA, locationB, locationC], axis=0)
        
        # Calculate unique ID of the triangle and insert it into the dictionary
        sorted_points = np.sort([first, second, third])
        tr_id = tuple(sorted_points)
        
        # Calculate triangle edge IDs
        tr_edges_ids = get_edges_ids(first, second, third)
        
        # Compute all the quantities and allocate them
        tr_info = triangleInfo(locationA, locationB, locationC)
        
        # Store initial values in the dictionary
        tr_dict[tr_id] = {
            "tr_ID": tr_id,
            "vertices": np.array([locationA, locationB, locationC]),
            "vertices_ids": np.array([first, second, third]),
            "edge_ids": tr_edges_ids,
            "barycenter": tr_bary,
            "neighbors": [],
            "e1": tr_info[0],
            "e2": tr_info[1],
            "ce1": tr_info[3],
            "ce2": tr_info[4],
            "n": tr_info[2],
            "area": tr_info[5],
            "g": tr_info[6],
            "g_inv": tr_info[7]
        }
        
        normals.append(tr_info[2])
        tr_bary_list.append(tr_bary)
    
        # Add triangle's edges to edge dictionary and store triangle ID as a value
        add_edges_ids(edges_to_triangles, tr_edges_ids, tr_id)
    
    # Calculate direction of each point's difference vector from each other point (required for normal calculation)  
    tr_bary_list = np.array(tr_bary_list)
    point_dists = getPointDistances(tr_bary_list)  
    
    # Find edge neighbors of each triangle
    for tr, triangle_data in tr_dict.items():    
        # Calculate and insert edge neighbors of triangle tr 
        for edge_id in triangle_data["edge_ids"]:
            related_triangles = edges_to_triangles.get(edge_id, [])
            for related_tr_id in related_triangles:
                if related_tr_id != tr:
                    if related_tr_id not in triangle_data["neighbors"]:
                        triangle_data["neighbors"].append(related_tr_id)                
        
    # Flip normals if required
    avg_dot_prod = sum(np.dot(point_dists[i], normals[i]) for i in range(len(normals))) / len(normals)
    if avg_dot_prod < 0:
        for j in tr_dict:
            tr_dict[j]["n"] *= -1
    
    # Compute curvature analytically for all triangles and store in the dictionary
    set_curvatures(tr_dict, neighs_order)
    
    # Optional: save .vtk file with additional information
    if save_vtk:
        try:
            # VTK scalars
            area_vals, C_mean, C_gauss, evals_max, evals_min, evecs_max, evecs_min = [], [], [], [], [], [], []
            faulty_triangles = []
            counter = 0
            
            for tr in tr_dict.keys():
                area_vals.append(tr_dict[tr]["area"])
                # Only use values for triangles that have 3 neighbors
                if not np.isnan(tr_dict[tr]["C_mean"]):
                    C_mean.append(tr_dict[tr]["C_mean"])
                    C_gauss.append(tr_dict[tr]["C_gauss"])
                    evals_min.append(tr_dict[tr]["evals"][0])  # Append smallest (negative) eigenvalue
                    evecs_min.append(tr_dict[tr]["evecs"][0])  # Append associated eigenvector
                    evals_max.append(tr_dict[tr]["evals"][1])  # Append largest (positive) eigenvalue
                    evecs_max.append(tr_dict[tr]["evecs"][1])  # Append associated eigenvector
                else:
                    C_mean.append(np.nan)
                    C_gauss.append(np.nan)
                    faulty_triangles.append(counter)  # Store triangles with less than 3 neighbors    
                counter += 1
                 
            # Remove faulty triangles from tr_bary_list
            tr_bary_list_filtered = np.delete(tr_bary_list, faulty_triangles, axis=0)
                
            vtk_mesh.cell_data["area"] = np.array(area_vals)
            vtk_mesh.cell_data["C_mean"] = np.array(C_mean)
            vtk_mesh.cell_data["C_gauss"] = np.array(C_gauss)
            
            # Create a directory to save the files
            output_dir = vtk_name if vtk_name else "output"
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            
            # Save mesh with additional information
            vtk_mesh.save(os.path.join(output_dir, "mesh_with_info.vtk"))  
            
            # VTK vectors for eigenvalues and eigenvectors
            poly_eig_min = pv.PolyData(tr_bary_list_filtered)
            poly_eig_min["vectors"] = np.array(evecs_min)
            poly_eig_min["values"] = np.array(evals_min)
            
            poly_eig_max = pv.PolyData(tr_bary_list_filtered)
            poly_eig_max["vectors"] = np.array(evecs_max)
            poly_eig_max["values"] = np.array(evals_max)
            
            # Create line sources for glyph representation
            sqrt_a = np.sqrt(np.mean(area_vals)) / 3
            line_source_min = pv.Line(pointa=[-sqrt_a, 0, 0], pointb=[sqrt_a, 0, 0])
            line_source_max = pv.Line(pointa=[-sqrt_a, 0, 0], pointb=[sqrt_a, 0, 0])
            line_eig_min = poly_eig_min.glyph(orient='vectors', scale=False, factor=1, geom=line_source_min)
            line_eig_max = poly_eig_max.glyph(orient='vectors', scale=False, factor=1, geom=line_source_max)
            
            # Save eigenvalue/eigenvector VTK files
            line_eig_min.save(os.path.join(output_dir, 'C_eigen_min.vtk'))
            line_eig_max.save(os.path.join(output_dir, 'C_eigen_max.vtk'))
        except Exception as e:
            raise RuntimeError(f"Failed to save VTK files: {e}")
        
    return tr_dict
