#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 30 13:35:46 2024

@author: mazzei
"""

import curvature_functions as cv
import datetime

# Specify the path to your VTK file
vtk_mesh_path = "/Users/mazzei/Desktop/PHD/Triangulation/curvedtissuessheardecomposition-lodovico/Tests/sphere.vtk"

# Call the function to process the mesh
# Get static data dictionaries

try:
    
    """
    Core function: get_mesh_info(vtk_mesh_path, neighs_order=2, save_vtk=True, vtk_name=False)
        
    Reads a VTK mesh file, computes geometric and curvature information for each triangle,
    and optionally saves the updated mesh with additional information in a specified directory.

    Parameters:
    vtk_mesh_path (str): Path to the input VTK file.
    save_vtk (bool): Whether to save the updated mesh and curvature information to new VTK files.
    vtk_name (str or bool): Custom name for the output directory. If False, use default names.

    Returns:
    dict: Dictionary containing information about each triangle in the mesh.
    """
    
    start_time = datetime.datetime.now()
    tr_dict = cv.get_mesh_info(vtk_mesh_path, neighs_order=2, save_vtk=True, vtk_name=False)
    print("Mesh processing complete.")

    # Optionally, print out some details about the processed mesh
    print(f"Number of triangles processed: {len(tr_dict)}")
    
    print("Runtime" + " --- %s seconds ---" % (datetime.datetime.now() - start_time))
    
except Exception as e:
    print(f"An error occurred: {e}")
